CONDA_ENV := ./env/
CONDA_ACTIVATE := eval "$$(conda shell.bash hook)"; conda activate $(CONDA_ENV); export PYTHONPATH=`pwd`:$${PYTHONPATH}
SHELL := bash

.PHONY: $(CONDA_ENV) clean setup

$(CONDA_ENV): environment.yml
	conda create --force --prefix $(CONDA_ENV) --file environment.yml
	$(CONDA_ACTIVATE)

activate: $(CONDA_ENV)
	$(CONDA_ACTIVATE)

clam: setup
	git clone https://github.com/mahmoodlab/CLAM
	echo 'export PYTHONPATH=`pwd`:$$PYTHONPATH' >> ~/.bashrc
	source ~/.bashrc

openslide: setup openslide-dependencies
	git clone https://github.com/openslide/openslide
	cd openslide ; autoreconf -i ; ./configure ; make ; make install
	pip install openslide-python

openslide-dependencies: apt-get
	apt-get install -y libjpeg-dev \
					   zlib1g-dev \
					   libopenjp2-7-dev \
					   libtiff-dev \
					   libglib2.0-dev \
					   libcairo2-dev \
					   libgdk-pixbuf2.0-dev \
					   libxml2-dev \
					   libsqlite3-dev \
					   openslide-tools \
					   libtool \
					   automake \
					   pkg-config \
					   ffmpeg \
					   libsm6 \
					   libxext6

apt-get:
	apt-get update

clean:
	rm -rf env/

# sudo docker run -it ubuntu
# # apt-get update --allow-releaseinfo-change
# # apt-get install -y wget git make pip vim
# # wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/anaconda.sh && \
#         /bin/bash ~/anaconda.sh -b -p /opt/conda && \
#                 rm ~/anaconda.sh
#                 # conda install pip
# pip install opencv-python h5py
