import os
import sys
import h5py
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from cv2 import addWeighted
from PIL import Image, ImageDraw

from skimage.color import rgb2gray
from skimage.filters import gaussian, threshold_otsu
from skimage.morphology import closing, dilation, disk
from skimage.feature import blob_dog, blob_log
from skimage.color import rgb2hed, hed2rgb
from scipy.spatial import ConvexHull
from sklearn.metrics.pairwise import pairwise_distances
from functools import reduce
import gc
            

def write_flush(*text_args, stream=sys.stdout):
    stream.write(', '.join(map(str, text_args)) + '\n')
    stream.flush()
    return

def get_cells(input_img, gauss_sigma, min_sigma, max_sigma, threshold):

    filtered_img = gaussian(input_img, sigma=gauss_sigma)
    inv_img = 1 - filtered_img

    blobs = blob_log(inv_img, min_sigma=min_sigma, max_sigma=max_sigma,
                     threshold=threshold, exclude_border=True)

    blobs[:, 2] = blobs[:, 2] * np.sqrt(2)

    return blobs

def find_clusters(points):

    dists = pairwise_distances(points, points)

    H, W = dists.shape
    clusters = []

    def find_cluster(clusters, point):
        for cluster_idx, cluster in enumerate(clusters):
            if point in cluster:
                return cluster_idx
        return None

    for i in range(H):

        point_i = tuple(points[i])[::-1]  # PIL takes (x, y) coordinates

        for j in range(W):

            if j == i:
                continue

            if dists[i, j] <= 35:

                point_j = tuple(points[j])[::-1]  # PIL takes (x, y) coordinates
                cluster_i = find_cluster(clusters, point_i)
                cluster_j = find_cluster(clusters, point_j)

                if cluster_i is None and cluster_j is None:  # new cluster
                    clusters.append({point_i, point_j})

                elif cluster_i is None:  # i joins j
                    clusters[cluster_j].add(point_i)

                elif cluster_j is None:  # j joins i
                    clusters[cluster_i].add(point_j)

                else:  # merge
                    if cluster_i == cluster_j:
                        continue
                    merged_cluster = clusters[cluster_i].union(clusters[cluster_j])
                    cluster_i = clusters[cluster_i]
                    cluster_j = clusters[cluster_j]
                    clusters.remove(cluster_i)
                    clusters.remove(cluster_j)
                    clusters.append(merged_cluster)

    return clusters

def get_cluster_mask(clusters):

    mask = Image.new('1', (256, 256), 0)

    for cluster in clusters:
        if len(cluster) < 3:
            continue

        cluster_list = list(cluster)
        hull = ConvexHull(cluster_list)
        ordered_points = [tuple(cluster_list[idx]) for idx in hull.vertices]
        ImageDraw.Draw(mask).polygon(ordered_points, outline=1, fill=1)

    return np.array(mask)

def decompose_ihc(input_img):

    # Separate the stains from the IHC image
    ihc_hed = rgb2hed(input_img)

    # Create an RGB image for each of the stains
    null = np.zeros_like(ihc_hed[:, :, 0])
    ihc_h = hed2rgb(np.stack((ihc_hed[:, :, 0], null, null), axis=-1))
    ihc_e = hed2rgb(np.stack((null, ihc_hed[:, :, 1], null), axis=-1))
    ihc_d = hed2rgb(np.stack((null, null, ihc_hed[:, :, 2]), axis=-1))

    return ihc_h, ihc_e, ihc_d

def get_crops_image(img, cells, crop_size=48):

    crops = []
    bboxes = []

    for cell in cells:
        y, x = map(int, cell)
        top, bottom = y - crop_size // 2, y + crop_size // 2
        left, right = x - crop_size // 2, x + crop_size // 2
        crop = img[top:bottom, left:right]

        if crop.shape[0] == crop_size and crop.shape[1] == crop_size:
            crops.append(crop)
            bboxes.append([left, top, right, bottom])

    return crops, bboxes

def visualise_clusters(input_img, cluster_mask, blobs, file_name):

    mask_patch = np.repeat(255 * cluster_mask[..., None], 3, axis=2).astype('uint8')
    patch = addWeighted(input_img, 0.6, mask_patch, 0.4, 0)

    fig = plt.Figure(figsize=(8, 8))
    ax = fig.add_subplot(111)
    ax.imshow(input_img)

    for blob in blobs:
        y, x, r = map(int, blob)
        c = plt.Circle((x, y), r, color='red', linewidth=2, fill=False)
        ax.add_patch(c)

    fig.savefig(f'./library_outputs/clusters_{file_name}.png')
    plt.close()

def plot_all(input_img, full_mask, cancer_blobs, normal_blobs, file_name):

    fig = plt.Figure(figsize=(8, 8))
    ax = fig.add_subplot(111)
    ax.imshow(input_img)

    for blob in cancer_blobs:
        y, x, r = map(int, blob)
        if full_mask[y, x]:
            c = plt.Circle((x, y), r, color='blue', linewidth=2, fill=False)
            ax.add_patch(c)

    for blob in normal_blobs:

        y, x, r = map(int, blob)
        if not full_mask[y, x]:
            c = plt.Circle((x, y), r, color='red', linewidth=2, fill=False)
            ax.add_patch(c)

    fig.savefig(f'library_outputs/detected_cells_{file_name}.png')
    plt.close()

def process_ihc(patches_dir, output_dir):

    patches_files = os.listdir(patches_dir)

    for patch_file in patches_files:

        write_flush(patch_file)
        bbox_data = []

        x_images = h5py.File(os.path.join(patches_dir, patch_file), 'r')['x']

        write_flush(x_images.shape)

        for i, input_img in enumerate(x_images):

            ihc_h, _, ihc_d = decompose_ihc(input_img)
            ihc_d_gray = rgb2gray(ihc_d)

            filtered_img = gaussian(ihc_d_gray, sigma=1)
            T = threshold_otsu(filtered_img)
            mask = 1 - (filtered_img > T)
            full_mask = closing(mask, disk(9))

            gauss_sigma = 2
            min_sigma = 7
            max_sigma = 10
            threshold = 0.02

            cancer_blobs = get_cells(1 - ihc_d_gray, gauss_sigma, min_sigma, max_sigma, threshold)

            if np.mean(ihc_d_gray) > 0.9:
                full_mask *= 0

            ihc_h_gray = rgb2gray(ihc_h)

            gauss_sigma = 2
            min_sigma = 1
            max_sigma = 3
            threshold = 0.08

            normal_blobs = get_cells(ihc_h_gray, gauss_sigma, min_sigma, max_sigma, threshold)

            cancer_cells, normal_cells = [], []

            for blob in cancer_blobs:
                y, x, r = map(int, blob)
                if full_mask[y, x]:
                    cancer_cells.append((y, x))

            for blob in normal_blobs:
                y, x, r = map(int, blob)
                if not full_mask[y, x]:
                    normal_cells.append((y, x))

            if i % 100 == 0:
                plot_all(input_img, full_mask, cancer_blobs, normal_blobs, '%s_%d' % (patch_file, i))

            crops, bboxes = get_crops_image(input_img, normal_cells)
            bboxes = [[patch_file] + [i] + bbox + [0] for bbox in bboxes]
            bbox_data.extend(bboxes)

            crops, bboxes = get_crops_image(input_img, cancer_cells)
            bboxes = [[patch_file] + [i] + bbox + [1] for bbox in bboxes]
            bbox_data.extend(bboxes)

        columns = ['patch_file', 'tile', 'xmin', 'ymin', 'xmax', 'ymax', 'class']
        index = np.arange(len(bbox_data))
        file_name = '%s.csv' % patch_file.split('.')[0]
        pd.DataFrame(bbox_data, columns=columns, index=index).to_csv(os.path.join(output_dir, file_name))

def process_hes(patches_dir, masks_dir, output_dir):

    patches_files = os.listdir(patches_dir)

    for patch_file in patches_files:

        write_flush(patch_file)
        bbox_data = []

        x_images = h5py.File(os.path.join(patches_dir, patch_file), 'r')['x']
        y_images = h5py.File(os.path.join(masks_dir, patch_file), 'r')['y']

        write_flush(x_images.shape)
        write_flush(y_images.shape)

        for i, (input_img, mask) in enumerate(zip(x_images, y_images)):

#            if 'CAMELYON' in output_dir and np.random.rand() < 0.8:  # too many tiles, drop 4/5
#                continue

            ihc_h, _, _ = decompose_ihc(input_img)
            ihc_h_gray = rgb2gray(ihc_h)

            gauss_sigma = 2
            min_sigma = 1
            max_sigma = 2
            threshold = 0.1

            normal_blobs = get_cells(ihc_h_gray, gauss_sigma, min_sigma, max_sigma, threshold)
            #normal_cells = [(int(y), int(x)) for y, x, _ in normal_blobs]

            normal_points = normal_blobs[:, :2]
            if len(normal_points) < 2:
                continue

            normal_cell_clusters = find_clusters(normal_points)            
            if len(normal_cell_clusters) == 0:
                continue

            #cluster_mask = get_cluster_mask(normal_cell_clusters)
            try:
                cluster_mask = get_cluster_mask(normal_cell_clusters)
            except:
                write_flush('Problem with: %s' % patch_file)
                continue

            if i % 10 == 0:
                visualise_clusters(input_img, cluster_mask, normal_blobs, '%s_%d' % (patch_file, i))

            cluster_points = reduce(lambda x, y : x.union(y), normal_cell_clusters)
            isolated_points = list(filter(lambda point : not tuple(point)[::-1] in
                                          cluster_points, normal_points))

            normal_cells = []

            for cluster in normal_cell_clusters:
                if len(cluster) < 3:
                    for point in cluster:
                        isolated_points.append(point[::-1])
                else:
                    for point in cluster:
                        x, y = map(int, point)  # N.B. X, Y FORMAT BECAUSE OF PIL--SEE ABOVE
                        normal_cells.append((y, x))

            for point in isolated_points:
                y, x = map(int, point)
                cluster_mask[y, x] = 1

            selem = disk(10)
            dilated_cluster_mask = dilation(cluster_mask, selem)

            full_mask = mask * (1 - dilated_cluster_mask)
            masked_input_img = full_mask * ihc_h_gray

            gauss_sigma = 1
            min_sigma = 10
            max_sigma = 12
            threshold = 0.01

            cancer_blobs = get_cells(ihc_h_gray, gauss_sigma, min_sigma, max_sigma, threshold)
            cancer_cells = []

            for blob in cancer_blobs:

                y, x, r = map(int, blob)
                if full_mask[y, x]:
                    cancer_cells.append((y, x))

            if i % 100 == 0:
                plot_all(input_img, full_mask, cancer_blobs, normal_blobs, '%s_%d' % (patch_file, i))

            crops, bboxes = get_crops_image(input_img, normal_cells)
            bboxes = [[patch_file] + [i] + bbox + [0] for bbox in bboxes]
            bbox_data.extend(bboxes)

            crops, bboxes = get_crops_image(input_img, cancer_cells)
            bboxes = [[patch_file] + [i] + bbox + [1] for bbox in bboxes]
            bbox_data.extend(bboxes)

        columns = ['patch_file', 'tile', 'xmin', 'ymin', 'xmax', 'ymax', 'class']
        index = np.arange(len(bbox_data))
        file_name = '%s.csv' % patch_file.split('.')[0]
        pd.DataFrame(bbox_data, columns=columns, index=index).to_csv(os.path.join(output_dir, file_name))

if __name__ == '__main__':

    patches_dir = '/workdir/shared/cpm4c/stain_transfer_CAMELYON16_2/20x/HES/patches'
    masks_dir = '/workdir/shared/cpm4c/stain_transfer_CAMELYON16_2/20x/HES/masks'
    output_dir = './BALANCED_LIBRARIES/library_HES_CAMELYON'

    process_hes(patches_dir, masks_dir, output_dir)

    patches_dir = '/workdir/shared/cpm4c/stain_transfer_balanced/20x/HES/patches/'
    masks_dir = '/workdir/shared/cpm4c/stain_transfer_balanced/20x/HES/masks/'
    output_dir = './BALANCED_LIBRARIES/library_HES'

    process_hes(patches_dir, masks_dir, output_dir)

    patches_dir = '/workdir/shared/cpm4c/stain_transfer_balanced/20x/IHC/patches/'
    output_dir = './BALANCED_LIBRARIES/library_IHC'

    process_ihc(patches_dir, output_dir)

