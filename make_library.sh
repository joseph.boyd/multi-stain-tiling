#!/bin/bash
#SBATCH --job-name=make_library
##SBATCH --time=09:00:00
#SBATCH --output=./outputs/%x_%j.out
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
##SBATCH --gres=gpu:1
##SBATCH --partition=gpu
#SBATCH --partition=mem
#SBATCH --mem=100000

# Activate anaconda environment code
source activate $WORKDIR/miniconda3/envs/clam

python make_library.py
