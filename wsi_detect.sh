#!/bin/bash
#SBATCH --job-name=stain_transfer
#SBATCH --output=./outputs/%x_%j.out
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --gres=gpu:1
#SBATCH --partition=gpu
#SBATCH --mem=120000

# Activate anaconda environment code
source activate $WORKDIR/miniconda3/envs/pytorch

# Classification

python wsi_detect.py -s ./gr_files_patches.csv -m models/resnet18.torch -o /workdir/shared/cpm4c/stain_transfer_wsi_outputs/GR/test --step_size 96 --patch_size 96
rm /workdir/shared/cpm4c/stain_transfer_wsi_outputs/GR/test/patches/*

# Stain transfer

#python wsi_detect.py -s ./gr_files_patches.csv -m models/G_AB_CAMELYON_BEST.pth -o /workdir/shared/cpm4c/stain_transfer_wsi_outputs/GR/test --step_size 256 --patch_size 256
#rm /workdir/shared/cpm4c/stain_transfer_wsi_outputs/GR/test/patches/*

