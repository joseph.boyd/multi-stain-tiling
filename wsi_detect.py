import os
import sys
import h5py
import argparse

import torch
import numpy as np
import pandas as pd
import torchvision.models as models
from PIL import Image
from skimage.io import imsave
from skimage.transform import resize

from CLAM.wsi_core.WholeSlideImage import WholeSlideImage
from CLAM.wsi_core.wsi_utils import *
from models.model_he2ihc import *
from utils import *


def resnet18(fine_tune, nb_classes):

    # initialise network
    net = models.resnet18(pretrained=True)

    if not fine_tune:  # freeze feature extraction layers
        for param in net.parameters():
            param.requires_grad = False

    net.fc = nn.Linear(512, nb_classes)
    return net

def stitch_infer(hdf5_file_path, model_file_path, device, downscale=16, bg_color=(255,255,255)):

    file = h5py.File(hdf5_file_path, 'r')
    dset = file['imgs']
    coords = file['coords'][:]
    if 'downsampled_level_dim' in dset.attrs.keys():
        w, h = dset.attrs['downsampled_level_dim']
    else:
        w, h = dset.attrs['level_dim']
    dset = infer_dset(dset, model_file_path, device)
    write_flush('original size: {} x {}'.format(w, h))
    w = w // downscale
    h = h //downscale
    coords = (coords / downscale).astype(np.int32)
    write_flush('downscaled size for stiching: {} x {}'.format(w, h))
    write_flush('number of patches: {}'.format(len(dset)))
    img_shape = dset[0].shape
    write_flush('patch shape: {}'.format(img_shape))
    downscaled_shape = (img_shape[1] // downscale, img_shape[0] // downscale)

    if w*h > Image.MAX_IMAGE_PIXELS:
        raise Image.DecompressionBombError("Visualization Downscale %d is too large" % downscale)
    
    output = Image.new(size=(w,h), mode="RGB", color=bg_color)
    
    output = np.array(output)
    indices = np.arange(len(coords))
    total = len(indices)

    for idx in range(total):
        patch_id = indices[idx]
        patch = Image.fromarray(dset[patch_id])
        patch = np.array(patch.resize(downscaled_shape))
        coord = coords[patch_id]
        canvas_crop_shape = output[coord[1]:coord[1]+downscaled_shape[1], coord[0]:coord[0]+downscaled_shape[0], :3].shape[:2]
        output[coord[1]:coord[1]+downscaled_shape[1], coord[0]:coord[0]+downscaled_shape[0], :3] = patch[:canvas_crop_shape[0], :canvas_crop_shape[1], :]

    return Image.fromarray(output)

def infer_dset(tiles, model_file_path, device, bs=32):

    if 'resnet' in model_file_path:
        model = resnet18(fine_tune=False, nb_classes=2).to(device)
    else:
        model = GeneratorResNet((3, 256, 256), 6).to(device)

    model.load_state_dict(torch.load(model_file_path, map_location=device))
    model = model.eval()

    inferred = []

    for i in range(0, len(tiles), bs):
        inp = torch.FloatTensor((2 * (np.moveaxis(tiles[i:min(len(tiles), i + bs)], -1, 1) / 255.)) - 1).to(device)

        if 'resnet' in model_file_path:
            probs = nn.Softmax(dim=1)(model(inp).detach().cpu())[:, 0:1, None, None].numpy()
            heatmap = (255 * probs * np.ones_like(inp.cpu())).astype('uint8')
        else:
            heatmap = (127.5 + 127.5 * np.array(model(inp).detach().cpu())).astype('uint8')

        inferred.extend(heatmap)

    return np.moveaxis(np.array(inferred),1,-1)

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-s', help='CSV of slide paths', required=True)
    parser.add_argument('-m', help='Path to model')
    parser.add_argument('-o', help='Output path')
    parser.add_argument('--patch_size', default=256, type=int)
    parser.add_argument('--step_size', default=256, type=int)
    args = parser.parse_args()

    os.makedirs(os.path.join(args.o,'patches'), exist_ok=True)
    os.makedirs(os.path.join(args.o,'stitches'), exist_ok=True)

    seg_params = {'seg_level': 6, 'sthresh': 7, 'mthresh': 7, 'close': 4, 'use_otsu': True}
    filter_params = {'a_t': 16.0, 'a_h': 4.0, 'max_n_holes': 8}
    patching_params = {'white_thresh': 5, 'black_thresh': 40, 'use_padding': True, 'contour_fn': 'four_pt', 
                       'patch_level': 2, 'patch_size': args.patch_size, 'step_size': args.step_size,
                       'save_path': args.o+'/patches/', 'custom_downsample': 1}

    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    write_flush(device)

    df_slides = pd.read_csv(args.s)

    for i, row in df_slides.iterrows():

        hes_file = row['HES'].replace('\\', '')

        write_flush(f'Processing: {hes_file}')

        WSI = WholeSlideImage(hes_file)
        WSI.segmentTissue(**seg_params, filter_params=filter_params)
    
        hdf5_file_path = patching_params['save_path'] + WSI.name + '.h5'
        if not os.path.exists(hdf5_file_path):
            hdf5_file_path = WSI.createPatches_bag_hdf5(**patching_params, save_coord=True)

        tiles_h5 = h5py.File(hdf5_file_path, 'r')
        tiles = tiles_h5['imgs']

        result = np.array(stitch_infer(hdf5_file_path, args.m, device))
        hes_thumbnail = np.array(WSI.wsi.get_thumbnail(result.shape[1::-1]))
        hes_thumbnail = (255 * resize(hes_thumbnail, output_shape=result.shape)).astype('uint8')

        ihc_file = row['IHC'].replace('\\', '')
        if ihc_file == 'None':
            ihc_thumbnail = np.zeros_like(result)
        else:
            WSI = WholeSlideImage(ihc_file)
            ihc_thumbnail = np.array(WSI.wsi.get_thumbnail(result.shape[1::-1]))
            ihc_thumbnail = (255 * resize(ihc_thumbnail, output_shape=result.shape)).astype('uint8')

        annotation_file = row['Annotation'].replace('\\', '')

        if annotation_file == 'None':
            mask = np.zeros_like(result)
        else:
            mask = get_mask_hes(WSI.wsi, annotation_file, level=3)
            mask = (255 * resize(mask, output_shape=np.array(result).shape[:2])).astype('uint8')
            mask = np.dstack([mask, mask, mask])

        mosaic = np.vstack([np.hstack([hes_thumbnail, ihc_thumbnail]),
                                np.hstack([result, mask])])

        slide_id = row['Class'] + '_' + row['Patient'] + '_' + str(row['Block']) + '_' + row['Slice']
        imsave(os.path.join(args.o, 'stitches/', slide_id + '.png'), mosaic)

        os.remove(hdf5_file_path)

