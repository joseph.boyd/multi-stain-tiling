import argparse
import os
import numpy as np
import pandas as pd
import math
import itertools
import datetime
import time

import torchvision.transforms as transforms
from torchvision.utils import save_image, make_grid

from torch.utils.data import DataLoader
from torchvision import datasets
from torch.autograd import Variable

from models import *
from datasets import *
from utils import *
from augmentation import HEDaugmentation

import torch.nn as nn
import torch.nn.functional as F
import torch

import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle


parser = argparse.ArgumentParser()
parser.add_argument("--epoch", type=int, default=0, help="epoch to start training from")
parser.add_argument("--n_epochs", type=int, default=25, help="number of epochs of training")
parser.add_argument("--dataset_name", type=str, default="monet2photo", help="name of the dataset")
parser.add_argument("--data_dir_A", type=str, default="", help="name of the dataset")
parser.add_argument("--data_dir_B", type=str, default="", help="name of the dataset")
parser.add_argument("--hes_dir", type=str, default="", help="path to hes bounding box library")
parser.add_argument("--ihc_dir", type=str, default="", help="path to ihc bounding box library")
parser.add_argument("--batch_size", type=int, default=1, help="size of the batches")
parser.add_argument("--steps_per_epoch", type=int, default=200, help="batches per epoch")
parser.add_argument("--lr", type=float, default=0.0002, help="adam: learning rate")
parser.add_argument("--b1", type=float, default=0.5, help="adam: decay of first order momentum of gradient")
parser.add_argument("--b2", type=float, default=0.999, help="adam: decay of first order momentum of gradient")
parser.add_argument("--decay_epoch", type=int, default=12, help="epoch from which to start lr decay")
parser.add_argument("--n_cpu", type=int, default=8, help="number of cpu threads to use during batch generation")
parser.add_argument("--img_height", type=int, default=256, help="size of image height")
parser.add_argument("--img_width", type=int, default=256, help="size of image width")
parser.add_argument("--channels", type=int, default=3, help="number of image channels")
parser.add_argument("--sample_interval", type=int, default=100, help="interval between saving generator outputs")
parser.add_argument("--checkpoint_interval", type=int, default=-1, help="interval between saving model checkpoints")
parser.add_argument("--n_residual_blocks", type=int, default=9, help="number of residual blocks in generator")
parser.add_argument("--lambda_cyc", type=float, default=10.0, help="cycle loss weight")
parser.add_argument("--lambda_id", type=float, default=5.0, help="identity loss weight")
parser.add_argument("--lambda_roi", type=float, default=1.0, help="roi discriminator loss weight")
opt = parser.parse_args()
write_flush(opt)

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
print(device)

# Create sample and checkpoint directories
os.makedirs("images/%s" % opt.dataset_name, exist_ok=True)
os.makedirs("saved_models/%s" % opt.dataset_name, exist_ok=True)

# Losses
criterion_GAN = torch.nn.MSELoss()
criterion_cycle = torch.nn.L1Loss()
criterion_identity = torch.nn.L1Loss()

cuda = torch.cuda.is_available()

input_shape = (opt.channels, opt.img_height, opt.img_width)

# Initialize generator and discriminator
G_AB = GeneratorResNet(input_shape, opt.n_residual_blocks)
G_BA = GeneratorResNet(input_shape, opt.n_residual_blocks)
D_A = Discriminator(input_shape)
D_B = Discriminator(input_shape)
D_ROI_A = DiscriminatorROI()
D_ROI_B = DiscriminatorROI()

if cuda:
    G_AB = G_AB.cuda()
    G_BA = G_BA.cuda()
    D_A = D_A.cuda()
    D_B = D_B.cuda()
    D_ROI_A = D_ROI_A.cuda()
    D_ROI_B = D_ROI_B.cuda()

    criterion_GAN.cuda()
    criterion_cycle.cuda()
    criterion_identity.cuda()

if opt.epoch != 0:
    # Load pretrained models
    G_AB.load_state_dict(torch.load("saved_models/%s/G_AB_%d.pth" % (opt.dataset_name, opt.epoch)))
    G_BA.load_state_dict(torch.load("saved_models/%s/G_BA_%d.pth" % (opt.dataset_name, opt.epoch)))
    D_A.load_state_dict(torch.load("saved_models/%s/D_A_%d.pth" % (opt.dataset_name, opt.epoch)))
    D_B.load_state_dict(torch.load("saved_models/%s/D_B_%d.pth" % (opt.dataset_name, opt.epoch)))
    D_ROI_A.load_state_dict(torch.load("saved_models/%s/D_ROI_A_%d.pth" % (opt.dataset_name, opt.epoch)))
    D_ROI_B.load_state_dict(torch.load("saved_models/%s/D_ROI_B_%d.pth" % (opt.dataset_name, opt.epoch)))
else:
    # Initialize weights
    G_AB.apply(weights_init_normal)
    G_BA.apply(weights_init_normal)
    D_A.apply(weights_init_normal)
    D_B.apply(weights_init_normal)
    D_ROI_A.apply(weights_init_normal)
    D_ROI_B.apply(weights_init_normal)

# Optimizers
optimizer_G = torch.optim.Adam(
    itertools.chain(G_AB.parameters(), G_BA.parameters()), lr=opt.lr, betas=(opt.b1, opt.b2)
)
optimizer_D_A = torch.optim.Adam(D_A.parameters(), lr=opt.lr, betas=(opt.b1, opt.b2))
optimizer_D_B = torch.optim.Adam(D_B.parameters(), lr=opt.lr, betas=(opt.b1, opt.b2))
optimizer_D_A_ROI = torch.optim.Adam(D_ROI_A.parameters(), lr=opt.lr, betas=(opt.b1, opt.b2))
optimizer_D_B_ROI = torch.optim.Adam(D_ROI_B.parameters(), lr=opt.lr, betas=(opt.b1, opt.b2))

# Learning rate update schedulers
lr_scheduler_G = torch.optim.lr_scheduler.LambdaLR(
    optimizer_G, lr_lambda=LambdaLR(opt.n_epochs, opt.epoch, opt.decay_epoch).step
)
lr_scheduler_D_A = torch.optim.lr_scheduler.LambdaLR(
    optimizer_D_A, lr_lambda=LambdaLR(opt.n_epochs, opt.epoch, opt.decay_epoch).step
)
lr_scheduler_D_B = torch.optim.lr_scheduler.LambdaLR(
    optimizer_D_B, lr_lambda=LambdaLR(opt.n_epochs, opt.epoch, opt.decay_epoch).step
)
lr_scheduler_D_A_ROI = torch.optim.lr_scheduler.LambdaLR(
    optimizer_D_A_ROI, lr_lambda=LambdaLR(opt.n_epochs, opt.epoch, opt.decay_epoch).step
)
lr_scheduler_D_B_ROI = torch.optim.lr_scheduler.LambdaLR(
    optimizer_D_B_ROI, lr_lambda=LambdaLR(opt.n_epochs, opt.epoch, opt.decay_epoch).step
)

Tensor = torch.cuda.FloatTensor if cuda else torch.Tensor

# Buffers of previously generated samples
fake_A_buffer = ReplayBuffer()
fake_B_buffer = ReplayBuffer()

# Load HES

def create_listed_dfs_dict(image_dict, dfs_dict):

    listed_dfs_dict = {}

    for key in image_dict.keys():

        img = image_dict[key]
        dfs = dfs_dict[key]

        dfs_list = [dfs[dfs.tile == i] for i in range(img.shape[0])]
        listed_dfs_dict[key] = dfs_list

    return listed_dfs_dict

patches_dir = os.path.join(opt.data_dir_A, 'patches/')

patches_files = os.listdir(patches_dir)

hes_images = {file_name : h5py.File(os.path.join(patches_dir, file_name))['x'][()]
              for file_name in sorted(patches_files)}

hes_dfs = {file_name : pd.read_csv(os.path.join(opt.hes_dir, file_name.split('.')[0] + '.csv'), index_col=0)
           for file_name in patches_files}

hes_bboxes = create_listed_dfs_dict(hes_images, hes_dfs)

# Load IHC

patches_dir = os.path.join(opt.data_dir_B, 'patches/')

patches_files = os.listdir(patches_dir)

ihc_images = {file_name : h5py.File(os.path.join(patches_dir, file_name))['x'][()]
              for file_name in sorted(patches_files)}

ihc_dfs = {file_name : pd.read_csv(os.path.join(opt.ihc_dir, file_name.split('.')[0] + '.csv'), index_col=0)
           for file_name in patches_files}

ihc_bboxes = create_listed_dfs_dict(ihc_images, ihc_dfs)

# Data generators

def split_dicts(img_dict, bbox_dict):

    img_dict_tr = {}
    img_dict_te = {}
    bbox_dict_tr = {}
    bbox_dict_te = {}

    for key in img_dict.keys():

        x = img_dict[key]
        y = bbox_dict[key]

        if x.shape[0] < 2:  # too small to split
            continue

        # split each slide by tile
        xtr, xte, ytr, yte = train_test_split(x, y, test_size=0.1, random_state=42)

        img_dict_tr[key] = xtr
        img_dict_te[key] = xte

        bbox_dict_tr[key] = ytr
        bbox_dict_te[key] = yte

    return img_dict_tr, img_dict_te, bbox_dict_tr, bbox_dict_te

hes_images_tr, hes_images_te, hes_bboxes_tr, hes_bboxes_te = split_dicts(hes_images, hes_bboxes)
ihc_images_tr, ihc_images_te, ihc_bboxes_tr, ihc_bboxes_te = split_dicts(ihc_images, ihc_bboxes)

def draw_conditions(bboxes, dim):

    condition = torch.zeros((1, 2, dim, dim))
    noise = torch.zeros((1, 2, dim, dim))

    for i, bbox in enumerate(bboxes):
        xmin, ymin, xmax, ymax, cls = map(int, bbox)

        ymin = max(0, ymin + 5)
        xmin = max(0, xmin + 5)

        ymax = min(dim, ymax - 5)
        xmax = min(dim, xmax - 5)

        condition[0, cls, ymin:ymax, xmin:xmax] = 1

        z = torch.randn(1, 1, ymax-ymin, xmax-xmin)
        noise[0, cls, ymin:ymax, xmin:xmax] = z

    return condition, noise

def data_generator(img_dict, bbox_dict, nb_batch, nb_rois=64):

#    tiles_per_slide = np.array([img_dict[key].shape[0] for key in img_dict.keys()])
#    probs = tiles_per_slide / np.sum(tiles_per_slide)
    non_empty_tiles_per_slide = np.array(
        [sum([1 for bbox in bbox_dict[key] if not bbox.empty]) for key in img_dict.keys()])
    probs = non_empty_tiles_per_slide / np.sum(non_empty_tiles_per_slide)

    while True:

        def sample_tile(x_imgs, df_bboxes_list):
            nb_tiles = len(df_bboxes_list)
            non_empty_tiles = [i for i in range(nb_tiles) if not df_bboxes_list[i].empty]
            tile_idx = np.random.choice(non_empty_tiles)

            return x_imgs[tile_idx][np.newaxis], df_bboxes_list[tile_idx]

        def sample_bboxes(df_bboxes, nb_samples):

            df_neg = df_bboxes[df_bboxes['class'] == 0]
            df_pos = df_bboxes[df_bboxes['class'] == 1]

            if df_neg.shape[0] == 0:
                df_sample = df_pos.sample(nb_samples, replace=True)
            elif df_pos.shape[0] == 0:
                df_sample = df_neg.sample(nb_samples, replace=True)
            else:
                df_sample = pd.concat([df_pos.sample(nb_samples // 2, replace=True),
                                       df_neg.sample(nb_samples // 2, replace=True)])

            return df_sample[['xmin', 'ymin', 'xmax', 'ymax', 'class']].values

        batch_slides = np.random.choice(list(img_dict.keys()), size=nb_batch, p=probs)
        data = [sample_tile(img_dict[slide], bbox_dict[slide]) for slide in batch_slides]

        x_batch = np.vstack([img for img, _ in data])
        x_batch = np.moveaxis(x_batch, 3, 1) / 127.5 - 1

        nb_samples = nb_rois // nb_batch
        bbox_data = [sample_bboxes(df_bbox, nb_samples) for _, df_bbox in data]
        bbox_batch = np.vstack([np.hstack([i * np.ones((nb_samples, 1)), bboxes])
                                for i, bboxes in enumerate(bbox_data)])

        condition_batch = []
        dim = x_batch.shape[2]

        for i in range(nb_batch):

            bboxes = bbox_batch[bbox_batch[:, 0]==i]
            condition, noise = draw_conditions(bboxes[:, 1:], dim)

            condition_batch.append(condition)

        condition_batch = torch.cat(condition_batch, axis=0)

        yield torch.Tensor(x_batch), condition_batch, torch.Tensor(bbox_batch)

"""
N.B. There is generally on a few hundred samples in the val/test data.
Hence, drawing 25 samples leads to duplicates with high probability
(see the birthday paradox). Furthermore, the duplicates will always
be consecutive in the batch, as the indices are sorted in the data_generator
function.
"""

def sample_images(batches_done):
    """Saves a generated sample from the test set"""
#    imgs = next(iter(val_dataloader))

    gen_A = data_generator(hes_images_te, hes_bboxes_te, nb_batch=25)
    gen_B = data_generator(ihc_images_te, ihc_bboxes_te, nb_batch=25)

    real_A = next(gen_A)[0].to(device)
    real_B = next(gen_B)[0].to(device)
    G_AB.eval()
    G_BA.eval()
    fake_B = G_AB(real_A)
    fake_A = G_BA(real_B)
    # Arrange images along x-axis
    real_A = make_grid(real_A, nrow=5, normalize=True)
    real_B = make_grid(real_B, nrow=5, normalize=True)
    fake_A = make_grid(fake_A, nrow=5, normalize=True)
    fake_B = make_grid(fake_B, nrow=5, normalize=True)
    # Arange images along y-axis
    image_grid = torch.cat((real_A, fake_B, real_B, fake_A), 1)
    save_image(image_grid, "images/%s/%s.png" % (opt.dataset_name, batches_done), normalize=False)

# ----------
#  Training
# ----------

nb_batch = 8
nb_rois = 64

gen_A = data_generator(hes_images_tr, hes_bboxes_tr, nb_batch=nb_batch, nb_rois=nb_rois)
gen_B = data_generator(ihc_images_tr, ihc_bboxes_tr, nb_batch=nb_batch, nb_rois=nb_rois)

aug = HEDaugmentation(nb_batch, 0.1).to(device)

prev_time = time.time()

for epoch in range(opt.epoch, opt.n_epochs):

    for i in range(opt.steps_per_epoch):

        data = next(gen_A)
        real_A, condition_A, bboxes_A = (data[0].to(device),
                                         data[1].to(device),
                                         data[2].to(device))

        data = next(gen_B)
        real_B, condition_B, bboxes_B = (data[0].to(device),
                                         data[1].to(device),
                                         data[2].to(device))

        fake = torch.zeros((nb_batch, *D_A.output_shape)).to(device)
        valid = torch.ones((nb_batch, *D_A.output_shape)).to(device)

        fake_roi = torch.zeros((nb_rois,)).to(device)
        valid_roi = torch.ones((nb_rois,)).to(device)

        # ------------------
        #  Train Generators
        # ------------------

        G_AB.train()
        G_BA.train()

        optimizer_G.zero_grad()

        # Identity loss
        loss_id_A = criterion_identity(G_BA(real_A), real_A)
        loss_id_B = criterion_identity(G_AB(real_B), real_B)

        loss_identity = (loss_id_A + loss_id_B) / 2

        # GAN loss
        real_A_aug = aug(real_A)
        fake_B = G_AB(real_A_aug)
        loss_GAN_AB = criterion_GAN(D_B(fake_B), valid)
        fake_A = G_BA(real_B)
        loss_GAN_BA = criterion_GAN(D_A(fake_A), valid)

        loss_GAN = (loss_GAN_AB + loss_GAN_BA) / 2

        # ROI loss
        validity_ROI_A = D_ROI_A(fake_A, condition_B, bboxes_B)
        validity_ROI_B = D_ROI_B(fake_B, condition_A, bboxes_A)

        loss_ROI_A = criterion_GAN(validity_ROI_A, valid_roi)
        loss_ROI_B = criterion_GAN(validity_ROI_B, valid_roi)

        loss_ROI = (loss_ROI_A + loss_ROI_B) / 2

        # Cycle loss
        recov_A = G_BA(fake_B)
        loss_cycle_A = criterion_cycle(recov_A, real_A)
        recov_B = G_AB(fake_A)
        loss_cycle_B = criterion_cycle(recov_B, real_B)

        loss_cycle = (loss_cycle_A + loss_cycle_B) / 2

        # Total loss
        loss_G = loss_GAN + opt.lambda_roi * loss_ROI + opt.lambda_cyc * loss_cycle + opt.lambda_id * loss_identity

        loss_G.backward()
        optimizer_G.step()

        # -----------------------
        #  Train Discriminator A
        # -----------------------

        optimizer_D_A.zero_grad()

        # Real loss
        loss_real = criterion_GAN(D_A(real_A), valid)
        # Fake loss (on batch of previously generated samples)
        fake_A_ = fake_A_buffer.push_and_pop(fake_A)
        loss_fake = criterion_GAN(D_A(fake_A_.detach()), fake)
        # Total loss
        loss_D_A = (loss_real + loss_fake) / 2

        loss_D_A.backward()
        optimizer_D_A.step()

        # -----------------------
        #  Train Discriminator B
        # -----------------------

        optimizer_D_B.zero_grad()

        # Real loss
        loss_real = criterion_GAN(D_B(real_B), valid)
        # Fake loss (on batch of previously generated samples)
        fake_B_ = fake_B_buffer.push_and_pop(fake_B)
        loss_fake = criterion_GAN(D_B(fake_B_.detach()), fake)
        # Total loss
        loss_D_B = (loss_real + loss_fake) / 2

        loss_D_B.backward()
        optimizer_D_B.step()

        loss_D = (loss_D_A + loss_D_B) / 2

        # --------------------------
        #  Train Discriminator ROI A
        # --------------------------

        optimizer_D_A_ROI.zero_grad()

        roi_outputs = D_ROI_A(real_A, condition_A, bboxes_A)
        real_loss = criterion_GAN(roi_outputs, valid_roi)

        roi_outputs = D_ROI_A(fake_A.detach(), condition_B, bboxes_B)
        fake_loss = criterion_GAN(roi_outputs, fake_roi)

        d_ROI_A_loss = (real_loss + fake_loss) / 2
        d_ROI_A_loss.backward()
        optimizer_D_A_ROI.step()

        # --------------------------
        #  Train Discriminator ROI B
        # --------------------------

        optimizer_D_B_ROI.zero_grad()

        roi_outputs = D_ROI_B(real_B, condition_B, bboxes_B)
        real_loss = criterion_GAN(roi_outputs, valid_roi)

        roi_outputs = D_ROI_B(fake_B.detach(), condition_A, bboxes_A)
        fake_loss = criterion_GAN(roi_outputs, fake_roi)

        d_ROI_B_loss = (real_loss + fake_loss) / 2
        d_ROI_B_loss.backward()
        optimizer_D_B_ROI.step()

        # --------------
        #  Log Progress
        # --------------

        # Determine approximate time left
        batches_done = epoch * opt.steps_per_epoch + i
        batches_left = opt.n_epochs * opt.steps_per_epoch - batches_done
        time_left = datetime.timedelta(seconds=batches_left * (time.time() - prev_time))
        prev_time = time.time()

        # Print log
        sys.stdout.write(
            "\r[Epoch %d/%d] [Batch %d/%d] [D loss: %f] [D_ROI_A loss: %f] [D_ROI_B loss: %f] [G loss: %f, adv: %f, cycle: %f, identity: %f] ETA: %s"
            % (
                epoch,
                opt.n_epochs,
                i,
                opt.steps_per_epoch,
                loss_D.item(),
                d_ROI_A_loss.item(),
                d_ROI_B_loss.item(),
                loss_G.item(),
                loss_GAN.item(),
                loss_cycle.item(),
                loss_identity.item(),
                time_left,
            )
        )

        # If at sample interval save image
        if batches_done % opt.sample_interval == 0:
            sample_images(batches_done)

    # Update learning rates
    lr_scheduler_G.step()
    lr_scheduler_D_A.step()
    lr_scheduler_D_B.step()
    lr_scheduler_D_A_ROI.step()
    lr_scheduler_D_B_ROI.step()

    if opt.checkpoint_interval != -1 and epoch % opt.checkpoint_interval == 0:
        # Save model checkpoints
        torch.save(G_AB.state_dict(), "saved_models/%s/G_AB_%d.pth" % (opt.dataset_name, epoch))
        torch.save(G_BA.state_dict(), "saved_models/%s/G_BA_%d.pth" % (opt.dataset_name, epoch))
        torch.save(D_A.state_dict(), "saved_models/%s/D_A_%d.pth" % (opt.dataset_name, epoch))
        torch.save(D_B.state_dict(), "saved_models/%s/D_B_%d.pth" % (opt.dataset_name, epoch))
        torch.save(D_ROI_A.state_dict(), "saved_models/%s/D_A_ROI_%d.pth" % (opt.dataset_name, epoch))
        torch.save(D_ROI_B.state_dict(), "saved_models/%s/D_B_ROI_%d.pth" % (opt.dataset_name, epoch))

