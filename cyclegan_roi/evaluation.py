import os
import argparse
from tqdm import tqdm
import h5py
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import torch

from pytorch_fid.fid_score import calculate_fid_given_tensors
from sklearn.model_selection import train_test_split

from models import GeneratorResNet
from utils import write_flush


parser = argparse.ArgumentParser()
parser.add_argument("--data_dir_A", type=str, default="", help="data path A")
parser.add_argument("--data_dir_B", type=str, default="", help="data path B")
parser.add_argument("--model_path", type=str, default="", help="path to model")
opt = parser.parse_args()

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
write_flush(device)

# Load model

g_proposed = GeneratorResNet((3, 256, 256), 9).to(device)
g_proposed.load_state_dict(torch.load(opt.model_path, map_location=device))
g_proposed = g_proposed.eval()

# Load data

masks_dir = os.path.join(opt.data_dir_A, 'masks/')
patches_dir = os.path.join(opt.data_dir_A, 'patches/')
patches_files = os.listdir(patches_dir)

hes_images = {file_name : h5py.File(os.path.join(patches_dir, file_name))['x'][()]
              for file_name in sorted(patches_files)}

hes_masks = {file_name : h5py.File(os.path.join(masks_dir, file_name))['y'][()]
             for file_name in sorted(patches_files)}

patches_dir = os.path.join(opt.data_dir_B, 'patches/')
patches_files = os.listdir(patches_dir)

ihc_images = {file_name : h5py.File(os.path.join(patches_dir, file_name))['x'][()]
              for file_name in sorted(patches_files)}

def split_dicts(img_dict, mask_dict):

    img_dict_tr = {}
    img_dict_te = {}
    mask_dict_tr = {}
    mask_dict_te = {}

    for key in img_dict.keys():

        x = img_dict[key]
        z = mask_dict[key]

        if x.shape[0] < 2:  # too small to split
            continue

        # split each slide by tile
        xtr, xte, ztr, zte = train_test_split(x, z, test_size=0.1, random_state=42)

        img_dict_tr[key] = xtr
        img_dict_te[key] = xte

        mask_dict_tr[key] = ztr
        mask_dict_te[key] = zte

    return img_dict_tr, img_dict_te, mask_dict_tr, mask_dict_te

hes_images_tr, hes_images_te, hes_masks_tr, hes_masks_te = split_dicts(hes_images, hes_masks)

reals = np.vstack(list(ihc_images.values()))
reals = torch.Tensor(np.moveaxis(reals, 3, 1)) / 127.5 - 1

imgs = np.vstack(list(hes_images.values()))
imgs = torch.Tensor(np.moveaxis(imgs, 3, 1)) / 127.5 - 1

fakes = torch.empty_like(imgs)


write_flush(reals.shape)
write_flush(fakes.shape)

with torch.no_grad():

    for i in tqdm(range(0, imgs.shape[0], 16)):

        samples = g_proposed(imgs[i:i+16].to(device)).detach()
        fakes[i:i+16] = (samples.detach().cpu() + 1) / 2  # [-1, 1] => [0, 1]
fid = calculate_fid_given_tensors([fakes, reals], 16, device, dims=2048)
write_flush('FID: %.04f' % fid)

