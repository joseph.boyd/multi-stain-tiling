import os
import h5py
import argparse

import torch
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split
from sklearn.metrics import jaccard_score, accuracy_score, balanced_accuracy_score, f1_score
from skimage.color import rgb2hed, hed2rgb
from skimage.filters import gaussian
from skimage.io import imsave
from skimage.color import rgb2gray
from skimage.filters import threshold_otsu
from skimage.morphology import closing, disk
from scipy.spatial.distance import dice

from utils import write_flush
from models import GeneratorResNet


parser = argparse.ArgumentParser()
parser.add_argument("--model_path", type=str, default="", help="path to model")
parser.add_argument("--data_dir_A", type=str, default="", help="name of the dataset")
parser.add_argument("--data_dir_B", type=str, default="", help="name of the dataset")
parser.add_argument("--hes_dir", type=str, default="", help="path to hes bounding box library")
parser.add_argument("--ihc_dir", type=str, default="", help="path to ihc bounding box library")
parser.add_argument("--n_residuals", type=int, default="", help="number of residual modules")
opt = parser.parse_args()
write_flush(opt)

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
print(device)

# Create sample and checkpoint directories
os.makedirs("jaccard/%s" % opt.model_path, exist_ok=True)

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
print(device)

# Load HES
def create_listed_dfs_dict(image_dict, dfs_dict):

    listed_dfs_dict = {}

    for key in image_dict.keys():

        img = image_dict[key]
        dfs = dfs_dict[key]

        dfs_list = [dfs[dfs.tile == i] for i in range(img.shape[0])]
        listed_dfs_dict[key] = dfs_list

    return listed_dfs_dict

# labels_dir = os.path.join(opt.data_dir_A, 'labels/')
masks_dir = os.path.join(opt.data_dir_A, 'masks/')
patches_dir = os.path.join(opt.data_dir_A, 'patches/')

patches_files = os.listdir(patches_dir)

hes_images = {file_name : h5py.File(os.path.join(patches_dir, file_name))['x'][()]
              for file_name in sorted(patches_files)}

hes_masks = {file_name : h5py.File(os.path.join(masks_dir, file_name))['y'][()]
             for file_name in sorted(patches_files)}

# hes_labels = {file_name : h5py.File(os.path.join(labels_dir, file_name))['y'][()]
#               for file_name in sorted(patches_files)}

hes_dfs = {file_name : pd.read_csv(os.path.join(opt.hes_dir, file_name.split('.')[0] + '.csv'), index_col=0)
           for file_name in patches_files}

hes_bboxes = create_listed_dfs_dict(hes_images, hes_dfs)

def split_dicts(img_dict, mask_dict, bbox_dict):

    img_dict_tr = {}
    img_dict_te = {}
    mask_dict_tr = {}
    mask_dict_te = {}
    bbox_dict_tr = {}
    bbox_dict_te = {}

    for key in img_dict.keys():

        x = img_dict[key]
        z = mask_dict[key]
        y = bbox_dict[key]

        if x.shape[0] < 2:  # too small to split
            continue

        # split each slide by tile
        xtr, xte, ztr, zte, ytr, yte = train_test_split(x, z, y, test_size=0.1, random_state=42)

        img_dict_tr[key] = xtr
        img_dict_te[key] = xte
        
        mask_dict_tr[key] = ztr
        mask_dict_te[key] = zte

        bbox_dict_tr[key] = ytr
        bbox_dict_te[key] = yte

    return img_dict_tr, img_dict_te, mask_dict_tr, mask_dict_te, bbox_dict_tr, bbox_dict_te

_, hes_images_te, _, hes_masks_te, _, hes_bboxes_te = split_dicts(hes_images, hes_masks, hes_bboxes)

g_proposed = GeneratorResNet((3, 256, 256), opt.n_residuals)
g_proposed.load_state_dict(torch.load(opt.model_path,
                                      map_location=device))
g_proposed = g_proposed.eval()

imgs = np.vstack(list(hes_images_te.values()))
imgs = torch.Tensor(np.moveaxis(imgs, 3, 1)) / 127.5 - 1

masks = np.vstack(list(hes_masks_te.values()))

def prep_for_pyplot(img):
    img = img.permute((1, 2, 0)).detach()
    img = (img + 1) / 2
    return img

def decompose_ihc(input_img):

    # Separate the stains from the IHC image
    ihc_hed = rgb2hed(input_img)

    # Create an RGB image for each of the stains
    null = np.zeros_like(ihc_hed[:, :, 0])
    ihc_h = hed2rgb(np.stack((ihc_hed[:, :, 0], null, null), axis=-1))
    ihc_e = hed2rgb(np.stack((null, ihc_hed[:, :, 1], null), axis=-1))
    ihc_d = hed2rgb(np.stack((null, null, ihc_hed[:, :, 2]), axis=-1))

    return ihc_h, ihc_e, ihc_d


overall = {'jaccard' : [], 'dice' : [], 'f1' : [], 'accuracy' : [], 'balanced_accuracy' : []}
negatives = {'jaccard' : [], 'dice' : [], 'f1' : [], 'accuracy' : [], 'balanced_accuracy' : []}
borders = {'jaccard' : [], 'dice' : [], 'f1' : [], 'accuracy' : [], 'balanced_accuracy' : []}
positives = {'jaccard' : [], 'dice' : [], 'f1' : [], 'accuracy' : [], 'balanced_accuracy' : []}

with torch.no_grad():

    for idx, (img, gt_mask) in enumerate(zip(imgs, masks)):
        proposed_pred = g_proposed(img[None])[0]

#        imsave('jaccard/%02d_input_img.png' % idx, prep_for_pyplot(img))
#        imsave('jaccard/%02d_mask.png' % idx, gt_mask)
#        imsave('jaccard/%02d_proposed.png' % idx, prep_for_pyplot(proposed_pred))

        input_img = prep_for_pyplot(proposed_pred)
        _, _, ihc_d = decompose_ihc(input_img)

        ihc_d_gray = rgb2gray(ihc_d)

        filtered_img = gaussian(ihc_d_gray, sigma=1)
        T = threshold_otsu(filtered_img)
        mask = 1 - (filtered_img > T)
        full_mask = closing(mask, disk(9))

        if np.mean(ihc_d_gray) > 0.9:
            full_mask *= 0

#        imsave('jaccard/%02d_mask_proposed.png' % idx, full_mask)

        gauss_sigma = 2
        min_sigma = 7
        max_sigma = 10
        threshold = 0.02

        jaccard = jaccard_score(full_mask.flatten(), gt_mask.flatten(), average="weighted")
        ds = 1 - dice(full_mask.flatten(), gt_mask.flatten())

        if np.isnan(ds):
            ds = 1

        f1 = f1_score(full_mask.flatten(), gt_mask.flatten())
        acc = accuracy_score(full_mask.flatten(), gt_mask.flatten())
        balanced_acc = balanced_accuracy_score(full_mask.flatten(), gt_mask.flatten())

        overall['jaccard'].append(jaccard)
        overall['dice'].append(ds)
        overall['f1'].append(f1)
        overall['accuracy'].append(acc)
        overall['balanced_accuracy'].append(balanced_acc)
       
        proportion = np.mean(gt_mask) 

        if proportion < 0.05:
            negatives['jaccard'].append(jaccard)
            negatives['dice'].append(ds)
            negatives['f1'].append(f1)
            negatives['accuracy'].append(acc)
            negatives['balanced_accuracy'].append(balanced_acc)
        elif proportion < 0.5:
            borders['jaccard'].append(jaccard)
            borders['dice'].append(ds)
            borders['f1'].append(f1)
            borders['accuracy'].append(acc)
            borders['balanced_accuracy'].append(balanced_acc)
        else:
            positives['jaccard'].append(jaccard)
            positives['dice'].append(ds)
            positives['f1'].append(f1)
            positives['accuracy'].append(acc)
            positives['balanced_accuracy'].append(balanced_acc)

        if idx > 500:
            break
#        write_flush(f'[{idx} / {imgs.shape[0]}]', '%.04f' % jaccard)
#        if idx > 10:
#            break

for metric in ['jaccard', 'dice', 'f1', 'accuracy', 'balanced_accuracy']:

    write_flush('##############')
    write_flush(metric)
    write_flush('##############')

    o = overall[metric]
    n = negatives[metric]
    b = borders[metric]
    p = positives[metric]

    write_flush('[Overall] mean: %.04f, std: %.04f' % (np.mean(o), np.std(o)))
    write_flush('[Negatives] mean: %.04f, std: %.04f' % (np.mean(n), np.std(n)))
    write_flush('[Borders] mean: %.04f, std: %.04f' % (np.mean(b), np.std(b)))
    write_flush('[Positives] mean: %.04f, std: %.04f' % (np.mean(p), np.std(p)))

