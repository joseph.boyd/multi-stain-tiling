#!/bin/bash
#SBATCH --job-name=jaccard
#SBATCH --output=./outputs/%x_%j.out
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --gres=gpu:1
#SBATCH --partition=gpu
#SBATCH --mem=120000

# Activate anaconda environment code
source activate $WORKDIR/miniconda3/envs/pytorch

#python -u jaccard.py --hes_dir ../library_HES --ihc_dir ../library_IHC_OLD --data_dir_A /workdir/shared/cpm4c/stain_transfer/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --n_residuals 9 --model_path ./saved_models/gr_l30_r9/G_AB_20.pth

# GR

#python -u jaccard.py --hes_dir ../library_HES --ihc_dir ../library_IHC_OLD --data_dir_A /workdir/shared/cpm4c/stain_transfer/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --n_residuals 6 --model_path ./saved_models/gr_l10_r6/G_AB_20.pth

#python -u jaccard.py --hes_dir ../library_HES --ihc_dir ../library_IHC_OLD --data_dir_A /workdir/shared/cpm4c/stain_transfer/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --n_residuals 9 --model_path ./saved_models/gr_l10_r9/G_AB_20.pth

#python -u jaccard.py --hes_dir ../library_HES --ihc_dir ../library_IHC_OLD --data_dir_A /workdir/shared/cpm4c/stain_transfer/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --n_residuals 6 --model_path ./saved_models/gr_l1_r6/G_AB_20.pth

#python -u jaccard.py --hes_dir ../library_HES --ihc_dir ../library_IHC_OLD --data_dir_A /workdir/shared/cpm4c/stain_transfer/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --n_residuals 9 --model_path ./saved_models/gr_l1_r9/G_AB_20.pth

#python -u jaccard.py --hes_dir ../library_HES --ihc_dir ../library_IHC_OLD --data_dir_A /workdir/shared/cpm4c/stain_transfer/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --n_residuals 6 --model_path ./saved_models/gr_l30_r6/G_AB_20.pth

#python -u jaccard.py --hes_dir ../library_HES --ihc_dir ../library_IHC_OLD --data_dir_A /workdir/shared/cpm4c/stain_transfer/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --n_residuals 9 --model_path ./saved_models/gr_l30_r9/G_AB_20.pth

# CAMELYON

#python -u jaccard.py --hes_dir ../library_HES_CAMELYON --ihc_dir ../library_IHC_OLD --data_dir_A /workdir/shared/cpm4c/stain_transfer_CAMELYON16/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --n_residuals 6 --model_path ./saved_models/camelyon_l10_r6/G_AB_20.pth

#python -u jaccard.py --hes_dir ../library_HES_CAMELYON --ihc_dir ../library_IHC_OLD --data_dir_A /workdir/shared/cpm4c/stain_transfer_CAMELYON16/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --n_residuals 9 --model_path ./saved_models/camelyon_l10_r9/G_AB_20.pth

#python -u jaccard.py --hes_dir ../library_HES_CAMELYON --ihc_dir ../library_IHC_OLD --data_dir_A /workdir/shared/cpm4c/stain_transfer_CAMELYON16/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --n_residuals 6 --model_path ./saved_models/camelyon_l10_r6/G_AB_20.pth

#python -u jaccard.py --hes_dir ../library_HES_CAMELYON --ihc_dir ../library_IHC_OLD --data_dir_A /workdir/shared/cpm4c/stain_transfer_CAMELYON16/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --n_residuals 9 --model_path ./saved_models/camelyon_l10_r9/G_AB_20.pth

python -u jaccard.py --hes_dir ../library_HES_CAMELYON --ihc_dir ../library_IHC_OLD --data_dir_A /workdir/shared/cpm4c/stain_transfer_CAMELYON16/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --n_residuals 6 --model_path ./saved_models/camelyon_l30_r6/G_AB_20.pth

python -u jaccard.py --hes_dir ../library_HES_CAMELYON --ihc_dir ../library_IHC_OLD --data_dir_A /workdir/shared/cpm4c/stain_transfer_CAMELYON16/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --n_residuals 9 --model_path ./saved_models/camelyon_l30_r9/G_AB_20.pth



