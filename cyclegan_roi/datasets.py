import glob
import random
import os

import h5py
import numpy as np
import torch
from torch.utils.data import Dataset
from PIL import Image
import torchvision.transforms as transforms


def load_data(root_dir):
    data = h5py.File(os.path.join(root_dir, 'x_train.h5'), 'r')
    x_train = data['x'][()]
    data.close()

    data = h5py.File(os.path.join(root_dir, 'x_valid.h5'), 'r')
    x_val = data['x'][()]
    data.close()

    data = h5py.File(os.path.join(root_dir, 'y_train.h5'), 'r')
    y_train = data['y'][()]
    data.close()

    data = h5py.File(os.path.join(root_dir, 'y_valid.h5'), 'r')
    y_val = data['y'][()]
    data.close()

    return x_train, x_val, y_train, y_val

def sample_data(x_data, nb_batch):
    # Pytorch randint trick (1000x faster than torch.randperm):
    x = torch.arange(x_data.shape[0])
    idx = x[torch.randint(0, x.shape[0], (nb_batch,))]
    x_batch = x_data[sorted(idx)]
    x_batch = np.moveaxis(x_batch, 3, 1).copy()
    x_batch = torch.Tensor(x_batch)
    x_batch = x_batch / 127.5 - 1  # range [-1, 1]

    return x_batch

def data_generator(x_data_A, x_data_B, nb_batch=32):
    while True:
        yield {'A' : sample_data(x_data_A, nb_batch), 'B' : sample_data(x_data_B, nb_batch)}


#def to_rgb(image):
#    rgb_image = Image.new("RGB", image.size)
#    rgb_image.paste(image)
#    return rgb_image
#
#
#class ImageDataset(Dataset):
#    def __init__(self, root, transforms_=None, unaligned=False, mode="train"):
#        self.transform = transforms.Compose(transforms_)
#        self.unaligned = unaligned
#
#        self.files_A = sorted(glob.glob(os.path.join(root, "%s/A" % mode) + "/*.*"))
#        self.files_B = sorted(glob.glob(os.path.join(root, "%s/B" % mode) + "/*.*"))
#
#    def __getitem__(self, index):
#        image_A = Image.open(self.files_A[index % len(self.files_A)])
#
#        if self.unaligned:
#            image_B = Image.open(self.files_B[random.randint(0, len(self.files_B) - 1)])
#        else:
#            image_B = Image.open(self.files_B[index % len(self.files_B)])
#
#        # Convert grayscale images to rgb
#        if image_A.mode != "RGB":
#            image_A = to_rgb(image_A)
#        if image_B.mode != "RGB":
#            image_B = to_rgb(image_B)
#
#        item_A = self.transform(image_A)
#        item_B = self.transform(image_B)
#        return {"A": item_A, "B": item_B}
#
#    def __len__(self):
#        return max(len(self.files_A), len(self.files_B))
