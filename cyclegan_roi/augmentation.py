import h5py
import math

import numpy as np
import torch
import torch.nn as nn


class HEDaugmentation(nn.Module):
    '''
    This is a tensorflow/keras preprocessing layer for H&E color augmentation based on the 
    hematoxylin and eosin deconvolution approach descripbed in:

    [1] Ruifrok AC, Johnston DA. Quantification of histochemical staining by color deconvolution. 
    Analytical & Quantitative Cytology & Histology 2001; 23: 291-299.
    augmentation inspiration came from:
    Tellez, David, et al. "Whole-slide mitosis detection in H&E breast histology using PHH3 as a 
    reference to train distilled stain-invariant convolutional networks." IEEE transactions on 
    medical imaging 37.9 (2018): 2126-2136.

    and

    [2] Tellez, David, et al. "Quantifying the effects of data augmentation and stain color 
    normalization in convolutional neural networks for computational pathology." Medical 
    image analysis 58 (2019): 101544.

    The expected input is a batch of images in RGB color space and in range [0,255].
    Input shape: [batch_size, width, height, 3]
    '''
    def __init__(self, nb_batch, sigma=0.01):
        super(HEDaugmentation, self).__init__()

        # M from [1] & https://blog.bham.ac.uk/intellimic/g-landini-software/colour-deconvolution-2/
        self.M = nn.Parameter(torch.Tensor([[0.651, 0.701, 0.290],
                                            [0.269, 0.568, 0.778],
                                            [0.633, -0.713, 0.302]])).requires_grad_(False)
        self.RGB2HED = nn.Parameter(torch.inverse(self.M)).requires_grad_(False)

        self.alpha = nn.Parameter(torch.empty(nb_batch, 1, 3)).requires_grad_(False)
        self.beta = nn.Parameter(torch.empty(nb_batch, 1, 3)).requires_grad_(False)
        self.sigma = sigma

    def forward(self, inputs):

        epsilon = math.pi
        inputs = 127.5 * (inputs + 1)  # [0, 255]

        # Reshaped images P \in R^(bs,N,3)
        P = torch.reshape(inputs, [-1, 3, inputs.shape[2] * inputs.shape[3]]).permute((0, 2, 1))

        # HED images
        S = -torch.log(P + epsilon) @ self.RGB2HED

        # Channel-wise pertubations
        self.alpha = self.alpha.normal_(1, self.sigma)  # mean 1
        self.beta = self.beta.normal_(0, self.sigma)    # mean 0

        S_hat = self.alpha * S + self.beta

        # Augmented RGB images
        P_hat = torch.exp(-(S_hat @ self.M)) - epsilon

        # Clip values to range [0, 255]
        P_hat_clipped = torch.clamp(P_hat, min=0.0, max=255.)
        P_hat_clipped = torch.reshape(P_hat_clipped.permute((0, 2, 1)), (-1, 3, inputs.shape[2], inputs.shape[3]))
        #Phat_uint8 = tf.cast(P_hat_clipped, tf.uint8)
        P_hat_clipped = P_hat_clipped / 127.5 - 1  # [-1, 1]
        return P_hat_clipped

# f = h5py.File("16H01396_03_NIV3_4.h5", "r")

# x = torch.Tensor(f["x"][()])
# x = x.permute(0, 3, 1, 2)[:8]

# aug = HEDaugmentation(nb_batch, sigma=0.1)
# y = aug(x)
# print(x.shape)

# import matplotlib.pyplot as plt
# fig, axes = plt.subplots(ncols=2)
# axes[0].imshow(x[0].permute((1, 2, 0)) / 255)
# axes[1].imshow(y[0].permute((1, 2, 0)) / 255)
# plt.show()
