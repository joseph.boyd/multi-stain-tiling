#!/bin/bash
#SBATCH --job-name=stain_transfer
#SBATCH --output=./outputs/%x_%j.out
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --gres=gpu:1
#SBATCH --partition=gpu
#SBATCH --mem=120000

# Activate anaconda environment code
source activate $WORKDIR/miniconda3/envs/pytorch

python -u main.py --dataset gr_l30_r9_aug --hes_dir ../library_HES --ihc_dir ../library_IHC_OLD --data_dir_A /workdir/shared/cpm4c/stain_transfer/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --checkpoint_interval 5 --sample_interval 100 --lambda_cyc 30.0 --lambda_roi 1.0 --n_residual_blocks 9

#python -u main.py --dataset gr_l30_r9 --hes_dir ../library_HES --ihc_dir ../library_IHC_OLD --data_dir_A /workdir/shared/cpm4c/stain_transfer/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --checkpoint_interval 2 --sample_interval 500 --lambda_cyc 30.0 --lambda_roi 1.0 --n_residual_blocks 9

#python -u main.py --dataset gr_l30_r6 --hes_dir ../library_HES --ihc_dir ../library_IHC_OLD --data_dir_A /workdir/shared/cpm4c/stain_transfer/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --checkpoint_interval 5 --sample_interval 500 --lambda_cyc 30.0 --lambda_roi 1.0 --n_residual_blocks 6

#python -u main.py --dataset gr_l1_r9 --hes_dir ../library_HES --ihc_dir ../library_IHC_OLD --data_dir_A /workdir/shared/cpm4c/stain_transfer/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --checkpoint_interval 5 --sample_interval 500 --lambda_cyc 1.0 --lambda_roi 1.0 --n_residual_blocks 9

#python -u main.py --dataset gr_l1_r6 --hes_dir ../library_HES --ihc_dir ../library_IHC_OLD --data_dir_A /workdir/shared/cpm4c/stain_transfer/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --checkpoint_interval 5 --sample_interval 500 --lambda_cyc 1.0 --lambda_roi 1.0 --n_residual_blocks 6

#python -u main.py --dataset camelyon_l30_r9 --hes_dir ../library_HES_CAMELYON --ihc_dir ../library_IHC_OLD --data_dir_A /workdir/shared/cpm4c/stain_transfer_CAMELYON16/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --checkpoint_interval 5 --sample_interval 500 --lambda_cyc 30.0 --lambda_roi 1.0 --n_residual_blocks 9

#python -u main.py --dataset camelyon_l30_r6 --hes_dir ../library_HES_CAMELYON --ihc_dir ../library_IHC_OLD --data_dir_A /workdir/shared/cpm4c/stain_transfer_CAMELYON16/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --checkpoint_interval 5 --sample_interval 500 --lambda_cyc 30.0 --lambda_roi 1.0 --n_residual_blocks 6

#python -u main.py --dataset camelyon_l1_r9 --hes_dir ../library_HES_CAMELYON --ihc_dir ../library_IHC_OLD --data_dir_A /workdir/shared/cpm4c/stain_transfer_CAMELYON16/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --checkpoint_interval 5 --sample_interval 500 --lambda_cyc 1.0 --lambda_roi 1.0 --n_residual_blocks 9

#python -u main.py --dataset camelyon_l1_r6 --hes_dir ../library_HES_CAMELYON --ihc_dir ../library_IHC_OLD --data_dir_A /workdir/shared/cpm4c/stain_transfer_CAMELYON16/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --checkpoint_interval 5 --sample_interval 500 --lambda_cyc 1.0 --lambda_roi 1.0 --n_residual_blocks 6

