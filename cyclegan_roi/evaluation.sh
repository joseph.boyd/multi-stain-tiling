#!/bin/bash
#SBATCH --job-name=evaluation
#SBATCH --output=./outputs/%x_%j.out
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --gres=gpu:1
#SBATCH --partition=gpu
##SBATCH --partition=cpu_short
#SBATCH --mem=250000

# Activate anaconda environment code
source activate $WORKDIR/miniconda3/envs/pytorch

#python evaluation.py --data_dir_A /workdir/shared/cpm4c/stain_transfer/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --model_path ./saved_models/gr_l10_r9/G_AB_20.pth
python evaluation.py --data_dir_A /workdir/shared/cpm4c/stain_transfer_CAMELYON16/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --model_path ./saved_models/camelyon_l30_r9/G_AB_20.pth
