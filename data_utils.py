import os
import sys

import h5py
import pickle
import torch
from torch import nn
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image, ImageDraw
import xml.etree.ElementTree as ET
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from skimage.filters import threshold_otsu


def load_data(base_dir):

    if 'CAMELYON' in base_dir:

        data = h5py.File(os.path.join(base_dir, 'camelyonpatch_level_2_split_train_x.h5'), 'r')
        x_train = data['x'][()]
        data.close()

        data = h5py.File(os.path.join(base_dir, 'camelyonpatch_level_2_split_valid_x.h5'), 'r')
        x_valid = data['x'][()]
        data.close()

        data = h5py.File(os.path.join(base_dir, 'camelyonpatch_level_2_split_test_x.h5'), 'r')
        x_test = data['x'][()]
        data.close()

        data = h5py.File(os.path.join(base_dir, 'camelyonpatch_level_2_split_train_y.h5'), 'r')
        y_train = data['y'][()].squeeze()
        data.close()

        data = h5py.File(os.path.join(base_dir, 'camelyonpatch_level_2_split_valid_y.h5'), 'r')
        y_valid = data['y'][()].squeeze()
        data.close()

        data = h5py.File(os.path.join(base_dir, 'camelyonpatch_level_2_split_test_y.h5'), 'r')
        y_test = data['y'][()].squeeze()
        data.close()

    else:

        patch_dir = os.path.join(base_dir, 'patches')
        label_dir = os.path.join(base_dir, 'labels')

        patch_files = sorted(os.listdir(patch_dir))
        label_files = sorted(os.listdir(label_dir))

        assert len(patch_files) == len(label_files)

        for patch_file, label_file in zip(patch_files, label_files):
            assert patch_file == label_file

        train_files, test_files = train_test_split(patch_files, test_size=5, random_state=42)
        train_files, val_files = train_test_split(train_files, test_size=5, random_state=42)

        def load_data(patch_dir, label_dir, split_files):

            patches = [h5py.File(os.path.join(patch_dir, file_name))['x'][()] for file_name in split_files]
            x_data = np.concatenate(patches)

            labels = [h5py.File(os.path.join(label_dir, file_name))['y'][()] for file_name in split_files]
            y_data = np.concatenate(labels)

            x_data, y_data = shuffle(x_data, y_data, random_state=0)

            return x_data, y_data

        x_train, y_train = load_data(patch_dir, label_dir, train_files)
        x_valid, y_valid = load_data(patch_dir, label_dir, val_files)
        x_test, y_test = load_data(patch_dir, label_dir, test_files)

    return x_train, y_train, x_valid, y_valid, x_test, y_test

def sample_batch(x_data, y_data, nb_batch, sample=True, augment=False):

    # sample data
    if sample:
        N = torch.arange(x_data.shape[0])
        idx = sorted(N[torch.randint(0, N.shape[0], (nb_batch,))])
    else:
        idx = torch.arange(x_data.shape[0])

    x_batch = x_data[idx]
    x_batch = torch.Tensor(np.moveaxis(x_batch, 3, 1).copy()) / 127.5 - 1
    y_batch = torch.Tensor(y_data[idx]).long()#.float()

    # stochastic augmentation
    if torch.rand(1) > 0.5 and augment:
        x_batch = torch.fliplr(x_batch)

    if torch.rand(1) > 0.5 and augment:
        x_batch = torch.flipud(x_batch)

    return x_batch, y_batch

def evaluate(model, x_data, y_data, criterion, device, epoch):

    N = x_data.shape[0]
    eval_loss, acc = 0, 0

    for i in range(0, N, 100):

        x_batch = x_data[i:min(N, i + 100)]
        y_batch = y_data[i:min(N, i + 100)]

        data = sample_batch(x_batch, y_batch, 100, sample=False)
        x_batch = data[0].to(device)
        y_batch = data[1].to(device)

        outputs = model(x_batch)
        loss = criterion(outputs, y_batch)
        eval_loss += (x_batch.shape[0] / N) * loss.item()
        predictions = torch.argmax(outputs, axis=1)
        acc += torch.sum(predictions == y_batch).item() / N

        if epoch % 5 == 0 and i == 0:  # visualise first batch every 5th epoch
            make_grid(x_batch, predictions[:100], y_batch[:100], epoch)

    return eval_loss, acc

def write_flush(*text_args, stream=sys.stdout):
    stream.write(', '.join(map(str, text_args)) + '\n')
    stream.flush()
    return

def save_obj(obj, name):
    with open(f'./pickles/{name}.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name):
    with open(f'./pickles/{name}.pkl', 'rb') as f:
        return pickle.load(f)

def save_fig(x, y, name, s=8):

    fig, axes = plt.subplots(figsize=(25, 25), ncols=s, nrows=s)

    for i in range(s ** 2):

        idx = np.random.randint(x.shape[0])
        axes[i // s][i % s].imshow(x[idx])
        axes[i // s][i % s].set_title(y[idx])
        axes[i // s][i % s].axis('off')

    fig.savefig(f'./figs/{name}.pdf', bbox_inches='tight')

def apply_otsu(img):
    t = threshold_otsu(img)
    return img > t

def parseNDPA(ndpa_filename, slide):
    '''
    Adapted for python from the groovy script to load .ndpa files to qupath retrieved from:
    https://forum.image.sc/t/importing-ndpa-annotation-files-into-qupath-code-attached/33721
    '''
    w, h = slide.dimensions
    micron2Pixel_X = 1/float(slide.properties['openslide.mpp-x'])
    micron2Pixel_Y = 1/float(slide.properties['openslide.mpp-y'])
    ImageCenter_X = (w/2)*1000/micron2Pixel_X    #nano unit
    ImageCenter_Y = (h/2)*1000/micron2Pixel_Y    #nano unit
    X_Reference = ImageCenter_X - float(slide.properties['hamamatsu.XOffsetFromSlideCentre'])
    Y_Reference = ImageCenter_Y - float(slide.properties['hamamatsu.YOffsetFromSlideCentre'])

    annotations = []
    for annot in ET.parse(ndpa_filename).findall('./ndpviewstate/annotation'):
        annotationType = annot.attrib['type']
        annotationColor = annot.attrib['color']
        if annotationType.lower() == 'freehand':
            isClosed = bool(int(annot.find('closed').text))
            specialType = getattr(annot.find('specialtype'), 'text', None)
            xx = np.asarray([float(x.text) for x in annot.findall('pointlist/point/x')])
            yy = np.asarray([float(y.text) for y in annot.findall('pointlist/point/y')])
            x = xx /1000 * micron2Pixel_X + X_Reference /1000 * micron2Pixel_X 
            y = yy /1000 * micron2Pixel_Y + Y_Reference /1000 * micron2Pixel_Y
            annotations.append({'type': annotationType, 
                                'color': annotationColor, 
                                'x':x, 'y':y,
                                'closed': isClosed,
                                'specialType': specialType})
        else:
            print('\033[93mParse Not Implemented for '+annotationType+'\033[0m')

    return annotations

def parseWFML(wfml_filename, slide):
    '''
    Adapted for python from the groovy script to load .ndpa files to qupath retrieved from:
    https://forum.image.sc/t/importing-ndpa-annotation-files-into-qupath-code-attached/33721
    '''
    w, h = slide.dimensions
    tree = ET.parse(wfml_filename)
    
    # Select wide-field with annotations
    wf = [i for i in tree.findall('./wide-field') if len(i.findall('./wide-field-description/'))>0][0]
    micron2Pixel_X = 1/float(slide.properties['openslide.mpp-x'])
    micron2Pixel_Y = 1/float(slide.properties['openslide.mpp-y'])
    offset_X = float(wf.findall('./wide-field-x')[0].text)
    offset_Y = float(wf.findall('./wide-field-y')[0].text)

    annotations = []
    for wfd in wf.findall('./wide-field-description/'):
        xx = np.asarray([float(x.text) for x in wfd.findall('./annotation-shape/shape-point/point-x')])
        yy = np.asarray([float(y.text) for y in wfd.findall('./annotation-shape/shape-point/point-y')])
        if len(xx)>0:
            x = xx*micron2Pixel_X - offset_X*micron2Pixel_X
            y = yy*micron2Pixel_Y - offset_Y*micron2Pixel_Y 

            annotations.append({'type': 'shape.polyline', 
                                'color': '#ff0000',
                                'x':x, 'y':y,
                                'closed': True,
                                'specialType': False})

    return annotations

def annot2mask(slide, annot, level):

    mask = Image.new('1', slide.level_dimensions[level], 0)

    for a in annot:
        x = a['x'] / slide.level_downsamples[level]
        y = a['y'] / slide.level_downsamples[level]

        if x.shape[0] < 2:  # contour must contain at least 2 points
            continue

        ImageDraw.Draw(mask).polygon(list(zip(x,y)), outline=1, fill=1)

    return np.array(mask)

def get_mask_hes(wsi, annotation_file, level):

    file_type = os.path.splitext(annotation_file)[-1]

    if file_type == '.ndpa':
        contours = parseNDPA(annotation_file, wsi)
    elif file_type == '.wfml':
        contours = parseWFML(annotation_file, wsi)

    mask = annot2mask(wsi, contours, level=level)
    return mask

def is_foreground(img):
    # improve segmentation by eliminating blanks
    corr = np.corrcoef(img[..., 0].flatten(), img[..., 1].flatten())
    if corr[0, 1] > 0.98:
        return False
    return True

#def parseNDPA(ndpa_filename, slide):
#    '''
#    Adapted for python from the groovy script to load .ndpa files to qupath retrieved from:
#    https://forum.image.sc/t/importing-ndpa-annotation-files-into-qupath-code-attached/33721
#    '''
#    w, h = slide.dimensions
#    micron2Pixel_X = 1/float(slide.properties['openslide.mpp-x'])
#    micron2Pixel_Y = 1/float(slide.properties['openslide.mpp-y'])
#    ImageCenter_X = (w/2)*1000/micron2Pixel_X    #nano unit
#    ImageCenter_Y = (h/2)*1000/micron2Pixel_Y    #nano unit
#    X_Reference = ImageCenter_X - float(slide.properties['hamamatsu.XOffsetFromSlideCentre'])
#    Y_Reference = ImageCenter_Y - float(slide.properties['hamamatsu.YOffsetFromSlideCentre'])
#
#    annotations = []
#    for annot in ET.parse(ndpa_filename).findall('./ndpviewstate/annotation'):
#        annotationType = annot.attrib['type']
#        annotationColor = annot.attrib['color']
#        if annotationType.lower() == 'freehand':
#            isClosed = bool(int(annot.find('closed').text))
#            specialType = getattr(annot.find('specialtype'), 'text', None)
#            xx = np.asarray([float(x.text) for x in annot.findall('pointlist/point/x')])
#            yy = np.asarray([float(y.text) for y in annot.findall('pointlist/point/y')])
#            x = xx /1000 * micron2Pixel_X + X_Reference /1000 * micron2Pixel_X
#            y = yy /1000 * micron2Pixel_Y + Y_Reference /1000 * micron2Pixel_Y
#            annotations.append({'type': annotationType,
#                                'color': annotationColor,
#                                'x':x, 'y':y,
#                                'closed': isClosed,
#                                'specialType': specialType})
#        else:
#            print('\033[93mParse Not Implemented for '+annotationType+'\033[0m')
#
#    return annotations
#
#def parseWFML(wfml_filename, slide):
#    '''
#    Adapted for python from the groovy script to load .ndpa files to qupath retrieved from:
#    https://forum.image.sc/t/importing-ndpa-annotation-files-into-qupath-code-attached/33721
#    '''
#    w, h = slide.dimensions
#    tree = ET.parse(wfml_filename)
#
#    # Select wide-field with annotations
#    wf = [i for i in tree.findall('./wide-field') if len(i.findall('./wide-field-description/'))>0][0]
#    micron2Pixel_X = 1/float(slide.properties['openslide.mpp-x'])
#    micron2Pixel_Y = 1/float(slide.properties['openslide.mpp-y'])
#    offset_X = float(wf.findall('./wide-field-x')[0].text)
#    offset_Y = float(wf.findall('./wide-field-y')[0].text)
#
#    annotations = []
#    for wfd in wf.findall('./wide-field-description/'):
#        xx = np.asarray([float(x.text) for x in wfd.findall('./annotation-shape/shape-point/point-x')])
#        yy = np.asarray([float(y.text) for y in wfd.findall('./annotation-shape/shape-point/point-y')])
#        if len(xx)>0:
#            x = xx*micron2Pixel_X - offset_X*micron2Pixel_X
#            y = yy*micron2Pixel_Y - offset_Y*micron2Pixel_Y
#
#            annotations.append({'type': 'shape.polyline',
#                                'color': '#ff0000',
#                                'x':x, 'y':y,
#                                'closed': True,
#                                'specialType': False})
#
#    return annotations
#
#def annot2mask(slide, annot, level):
#
#    mask = Image.new('1', slide.level_dimensions[level], 0)
#
#    for a in annot:
#        x = a['x'] / slide.level_downsamples[level]
#        y = a['y'] / slide.level_downsamples[level]
#
#        if x.shape[0] < 2:  # contour must contain at least 2 points
#            continue
#
#        ImageDraw.Draw(mask).polygon(list(zip(x,y)), outline=1, fill=1)
#
#    return np.array(mask)
#
#def init_weights(m):
#    if isinstance(m, nn.Conv2d):
#        torch.nn.init.xavier_uniform_(m.weight)
#    if isinstance(m, nn.Linear):
#        torch.nn.init.xavier_uniform_(m.weight)
#        m.bias.data.fill_(0.01)
#
#def learning_rate_schedule(epoch):
#    return 2 ** -(epoch // 20)
#
## Dice similarity function
#def dice(pred, true, k = 1):
#    intersection = np.sum(pred[true==k]) * 2.0
#    dice = intersection / (np.sum(pred) + np.sum(true))
#    return dice
#
#def make_grid(imgs, preds, classes, epoch):
#
#    fig, axes = plt.subplots(nrows=10, ncols=10, figsize=(25, 25))
#
#    for i in range(10):
#        for j in range(10):
#            img = 0.5 * imgs[10 * i + j].detach().cpu().permute((1, 2, 0)) + 0.5
#            cls = classes[10 * i + j]
#            pred = preds[10 * i + j]
#            axes[i][j].imshow(img)
#            axes[i][j].set_title('T%d - P%d' % (cls, pred))
#            axes[i][j].axis('off')
#
#    fig.tight_layout()
#    fig.savefig('./evaluation/%02d.pdf' % epoch, bbox_inches='tight')
#    plt.close()
#
#def save_obj(obj, name):
#    with open(f'./pickles/{name}.pkl', 'wb') as f:
#        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)
#
#def load_obj(name):
#    with open(f'./pickles/{name}.pkl', 'rb') as f:
#        return pickle.load(f)
#
#def save_fig(x, y, name, s=8):
#
#    fig, axes = plt.subplots(figsize=(25, 25), ncols=s, nrows=s)
#
#    for i in range(s ** 2):
#
#        idx = np.random.randint(x.shape[0])
#        axes[i // s][i % s].imshow(x[idx])
#        axes[i // s][i % s].set_title(y[idx])
#        axes[i // s][i % s].axis('off')
#
#    fig.savefig(f'./figs/{name}.pdf', bbox_inches='tight')
#
#def apply_otsu(img):
#    t = threshold_otsu(img)
#    return img > t
#
