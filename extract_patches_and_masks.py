import os
from os import path
import sys
import h5py
import argparse
import random
import pickle

import cv2
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from openslide import OpenSlide
from PIL import Image, ImageDraw
from skimage.transform import resize, rescale
from skimage.color import rgb2hsv
from skimage.io import imsave
from skimage.filters import threshold_otsu
from skimage.morphology import opening, disk
from sklearn.model_selection import train_test_split
from bs4 import BeautifulSoup
import xml.etree.ElementTree as ET

from utils import *


def extract_patches(df_patients, coords_directory, stain, tile_level, mask_level, save_path):

    for i, row in df_patients.iterrows():

        file_name = row[stain].replace('\\', '')
        write_flush(i, file_name)
        patches_file = row[stain + 'Patches']

        wsi = OpenSlide(file_name)
        coords = h5py.File(path.join(coords_directory, patches_file), 'r')['coords'][()]

        tiles = [np.array(wsi.read_region((c[0], c[1]), level=tile_level, size=(256, 256)))[..., :3]
                   for c in coords]

        if stain == 'HES':

            annotation_file = row['Annotation'].replace('\\', '')
            mask = get_mask_hes(wsi, annotation_file, level=mask_level)

        else:

            zipped = filter(lambda x : is_foreground(x[0]), zip(tiles, coords))
            tiles, coords = zip(*zipped)
            coords = np.array(coords)

            mask_thumbnail = np.array(wsi.get_thumbnail(size=(10000, 10000)))
            hsv_img = rgb2hsv(mask_thumbnail)

            h_mask = apply_otsu(hsv_img[..., 0])
            v_mask = apply_otsu(hsv_img[..., 2])

            seg = ~v_mask * ~h_mask
            mask = opening(seg, disk(1))

            mask_level = np.log2(wsi.dimensions[1] / mask.shape[0])

        #mask = rescale(mask, 0.1, anti_aliasing=False)
        #imsave('./masks/mask_%02d.png' % i, mask)

        ratio = 2 ** (mask_level - 0)                                # 2 ** 2 = 4
        size = int(round(256 / 2 ** (mask_level - tile_level)))      # 256 // 2 = 128

        mask_coords = np.round(coords[()] / ratio).astype('int')
        masks = [mask[t:t+size, l:l+size] for l, t in mask_coords]

        proportions = [np.sum(mask_image) / size ** 2 for mask_image in masks]

        positives = [(tile, mask) for tile, mask, prop in zip(tiles, masks, proportions) if prop >= 0.05]
        negatives = [(tile, mask) for tile, mask, prop in zip(tiles, masks, proportions) if prop < 0.05]

        median_proportion = np.median([prop for prop in proportions if prop >= 0.05])

        borders = [(tile, mask) for tile, mask, prop in zip(tiles, masks, proportions) if 0.05 <= prop and prop < median_proportion]
        positives = [(tile, mask) for tile, mask, prop in zip(tiles, masks, proportions) if prop >= median_proportion]

        nb_samples = min(500, min(len(positives), len(borders), len(negatives) // 2))

        if nb_samples == 0:
            continue

        negatives = random.choices(negatives, k=2 * nb_samples)
        borders = random.choices(borders, k=nb_samples)
        positives = random.choices(positives, k=nb_samples)

        patches = np.vstack([
            np.stack([neg for neg, _ in negatives]),
            np.stack([brd for brd, _ in borders]),
            np.stack([pos for pos, _ in positives])])

        masks = np.vstack([
            np.stack([resize(mask, order=0, output_shape=(256, 256)).astype('bool') for _, mask in negatives]),
            np.stack([resize(mask, order=0, output_shape=(256, 256)).astype('bool') for _, mask in borders]),
            np.stack([resize(mask, order=0, output_shape=(256, 256)).astype('bool') for _, mask in positives])])

        for ii in range(20):
            idx = np.random.randint(patches.shape[0])
            imsave('samples/%02d_patch.png' % ii, patches[idx])
            imsave('samples/%02d_mask.png' % ii, masks[idx])

        labels = 2 * nb_samples * [0] + 2 * nb_samples * [1]

        slide_id = row['Patient'] + '_' + str(row['Block']) + '_' + row['Slice']

        h5f = h5py.File(f'{save_path}/{stain}/patches/{slide_id}.h5', 'w')
        h5f.create_dataset('x', data=patches)
        h5f.close()

        h5f = h5py.File(f'{save_path}/{stain}/masks/{slide_id}.h5', 'w')
        h5f.create_dataset('y', data=masks)
        h5f.close()

        h5f = h5py.File(f'{save_path}/{stain}/labels/{slide_id}.h5', 'w')
        h5f.create_dataset('y', data=labels)
        h5f.close()


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Extract concentric tiles')
    parser.add_argument('metadata', type=str, default='/gpfs/workdir/shared/cpm4c/gr_files.csv')
    parser.add_argument('--coord_path', type=str)
    parser.add_argument('--save_path', type=str)
    parser.add_argument('--level', type=int)
    parser.add_argument('--mask_level', type=int)
    args = parser.parse_args()

    write_flush(str(args))

    df_gr = pd.read_csv(args.metadata)
    df_metastases = df_gr[~(df_gr['Annotation']=='None')]
    df_metastases = df_metastases[~(df_metastases['HES']=='None')]
    df_metastases = df_metastases[~(df_metastases['IHC']=='None')]
    df_metastases = df_metastases[~(df_metastases['HESPatches']=='None')]
    df_metastases = df_metastases[~(df_metastases['IHCPatches']=='None')]

    write_flush('No. slides: %d' % df_metastases.shape[0])

    hes_coord_path = f'{args.coord_path}/HES/patches'
    ihc_coord_path = f'{args.coord_path}/IHC/patches'

    #extract_patches(df_metastases, hes_coord_path, 'HES', args.level, args.mask_level, args.save_path)
    extract_patches(df_metastases, ihc_coord_path, 'IHC', args.level, args.mask_level, args.save_path)

