import os
import sys
import h5py
from random import shuffle

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

import torch
import torchvision.models as models
import torchvision.transforms as transforms
import torch.nn as nn
from torch.utils.data import Subset
from torch.autograd import Variable
import torch.optim as optim


def write_flush(*text_args, stream=sys.stdout):
    stream.write(', '.join(map(str, text_args)) + '\n')
    stream.flush()
    return

class CAMELYON:

    def __init__(self, root, train, download, transform):

        if train:
            f = h5py.File(os.path.join(root, 'camelyonpatch_level_2_split_train_x.h5'), 'r')
            self.images = f['x']
            f = h5py.File(os.path.join(root, 'camelyonpatch_level_2_split_train_y.h5'), 'r')
            self.labels = f['y']
        else:
            f = h5py.File(os.path.join(root, 'camelyonpatch_level_2_split_test_x.h5'), 'r')
            self.images = f['x']
            f = h5py.File(os.path.join(root, 'camelyonpatch_level_2_split_test_y.h5'), 'r')
            self.labels = f['y']

        self.classes = ['normal', 'metastatic']

    def __getitem__(self, idx):

        img, label = torch.Tensor(self.images[idx]), int(self.labels[idx])
        img = img / 127.5 - 1

        return img.permute((2, 0, 1)), label

    def __len__(self):
        return self.images.shape[0]

def get_dataloaders(dataset, nb_batch):

    transform = transforms.Compose([
        transforms.ToTensor(),
        GrayscaleToRGB(),
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

    trainset = dataset(root='/workdir/shared/cpm4c/PCam', train=True,
                       download=True, transform=transform)

    nb_train = len(trainset)
    nb_val = int(0.1 * nb_train)

    idx = list(range(nb_train))
    shuffle(idx)

    valset = Subset(trainset, idx[:nb_val])
    trainset = Subset(trainset, idx[nb_val:])

    trainloader = torch.utils.data.DataLoader(trainset, batch_size=nb_batch,
                                              shuffle=True, num_workers=4)
    valloader = torch.utils.data.DataLoader(valset, batch_size=nb_batch,
                                            shuffle=True, num_workers=4)

    testset = dataset(root='/workdir/shared/cpm4c/PCam', train=False,
                      download=True, transform=transform)
    testloader = torch.utils.data.DataLoader(testset, batch_size=nb_batch,
                                             shuffle=False, num_workers=4)

    return trainloader, valloader, testloader

def train_and_test(network, dataset, fine_tune, nb_epochs, nb_batch, weight_decay):

    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    criterion = nn.CrossEntropyLoss()

    trainloader, valloader, testloader = get_dataloaders(dataset, nb_batch)
    nb_classes = len(trainloader.dataset.dataset.classes)

    net = network(fine_tune, nb_classes).to(device)

    optimizer = optim.SGD(filter(lambda p: p.requires_grad, net.parameters()),
                          lr=0.0001, momentum=0.9, weight_decay=weight_decay)

    for epoch in range(nb_epochs):

        running_loss = 0.0

        for i, data in enumerate(trainloader, 0):
            inputs, labels = data
            inputs, labels = inputs.to(device), Variable(labels.to(device))

            optimizer.zero_grad()

            outputs = net(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            running_loss += loss.item()

            if i % 100 == 99:
                write_flush('[Train loss] %.04f' % (running_loss / 100))
                running_loss = 0.0

        correct, total = 0, 0

        net.eval()

        for data in testloader:
            images, labels = data
            outputs = net(Variable(images.to(device)))
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted.to(device) == labels.to(device)).sum()

        accuracy = (100 * correct / total).item()
        write_flush('[Val acc.] %.04f' % accuracy)

    torch.save(net.state_dict(), './model.torch')

def resnet18(fine_tune, nb_classes):

    # initialise network
    net = models.resnet18(pretrained=True)

    if not fine_tune:  # freeze feature extraction layers
        for param in net.parameters():
            param.requires_grad = False

    net.fc = nn.Linear(512, nb_classes)
    return net

def densenet161(fine_tune, nb_classes):

    # initialise network
    net = models.densenet161(pretrained=True)

    if not fine_tune:  # freeze feature extraction layers
        for param in net.parameters():
            param.requires_grad = False

    net.classifier = nn.Linear(2208, nb_classes)
    return net

if __name__ == '__main__':

    train_and_test(resnet18, CAMELYON, fine_tune=True, nb_epochs=1, nb_batch=32, weight_decay=1e-1)

