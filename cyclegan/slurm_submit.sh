#!/bin/bash
#SBATCH --job-name=stain_transfer
#SBATCH --output=./outputs/%x_%j.out
##SBATCH --time=00:30:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --gres=gpu:1
#SBATCH --partition=gpu
#SBATCH --mem=120000

# Activate anaconda environment code
source activate $WORKDIR/miniconda3/envs/pytorch

# execution
python -u main.py --batch_size 1 --dataset gr --data_dir_A /workdir/shared/cpm4c/stain_transfer/20x/HES --data_dir_B /workdir/shared/cpm4c/stain_transfer/20x/IHC --checkpoint_interval 10 --sample_interval 1000 #--lambda_cyc 5.0 #--epoch 190
