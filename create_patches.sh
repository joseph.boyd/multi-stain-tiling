#!/bin/bash
#SBATCH --job-name=clam
##SBATCH --time=06:00:00
#SBATCH --output=./outputs/%x_%j.out
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --partition=cpu_short
##SBATCH --gres=gpu:1
#SBATCH --mem=120000

# Activate anaconda environment code
source activate $WORKDIR/miniconda3/envs/clam

#########################
# CAMELYON17
#########################
# python create_patches_fp.py --source CAMELYON17_FILES --save_dir ./clam_outputs --patch_size 256 --seg --patch --stitch --preset camelyon.csv

#########################
# GR HES 40x 
#########################
python create_patches_fp.py --source /workdir/shared/cpm4c/GR_DATA/HES --save_dir ./clam_outputs/gr/patch_256/HES --patch_size 256 --step_size 256 --seg --patch --stitch --process_list process_list_HES.csv #--no_auto_skip
python create_patches_fp.py --source /workdir/shared/cpm4c/GR_DATA/IHC --save_dir ./clam_outputs/gr/patch_256/IHC --patch_size 256 --step_size 256 --seg --patch --stitch --process_list process_list_IHC.csv #--no_auto_skip

#########################
# GR HES 20x
#########################
python create_patches_fp.py --source /workdir/shared/cpm4c/GR_DATA/HES --save_dir ./clam_outputs/gr/patch_512/HES --patch_size 512 --step_size 512 --patch_level 1 --seg --patch --stitch --process_list process_list_HES.csv #--no_auto_skip
python create_patches_fp.py --source /workdir/shared/cpm4c/GR_DATA/IHC --save_dir ./clam_outputs/gr/patch_512/IHC --patch_size 512 --step_size 512 --patch_level 1 --seg --patch --stitch --process_list process_list_IHC.csv #--no_auto_skip

#########################
# GR HES 10x
#########################
python create_patches_fp.py --source /workdir/shared/cpm4c/GR_DATA/HES --save_dir ./clam_outputs/gr/patch_1024/HES --patch_size 1024 --step_size 1024 --patch_level 2 --seg --patch --stitch --process_list process_list_HES.csv #--no_auto_skip
python create_patches_fp.py --source /workdir/shared/cpm4c/GR_DATA/IHC --save_dir ./clam_outputs/gr/patch_1024/IHC --patch_size 1024 --step_size 1024 --patch_level 2 --seg --patch --stitch --process_list process_list_IHC.csv #--no_auto_skip

