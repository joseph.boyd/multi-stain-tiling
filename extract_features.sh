#!/bin/bash
#SBATCH --job-name=clam
#SBATCH --time=01:00:00
#SBATCH --output=./outputs/%x_%j.out
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --partition=gpu_test
#SBATCH --gres=gpu:1
#SBATCH --mem=120000

# Activate anaconda environment code
source activate $WORKDIR/miniconda3/envs/clam

echo "slide_id" > slides.csv
ls /workdir/shared/cpm4c/GR_DATA/HES | rev | cut -c 6- | rev >> slides.csv

#########################
# GR HES 40x 
#########################
python CLAM/extract_features_fp.py --data_h5_dir ./clam_outputs/gr/patch_1024/HES --data_slide_dir /workdir/shared/cpm4c/GR_DATA/HES --csv_path slides.csv --feat_dir ./clam_outputs/gr/patch_1024/HES --batch_size 32 --slide_ext .ndpi
python CLAM/extract_features_fp.py --data_h5_dir ./clam_outputs/gr/patch_1024/IHC --data_slide_dir /workdir/shared/cpm4c/GR_DATA/IHC --csv_path slides.csv --feat_dir ./clam_outputs/gr/patch_1024/IHC --batch_size 32 --slide_ext .ndpi

#########################
# GR HES 20x 
#########################

python CLAM/extract_features_fp.py --data_h5_dir ./clam_outputs/gr/patch_512/HES --data_slide_dir /workdir/shared/cpm4c/GR_DATA/HES --csv_path slides.csv --feat_dir ./clam_outputs/gr/patch_512/HES --batch_size 32 --slide_ext .ndpi
python CLAM/extract_features_fp.py --data_h5_dir ./clam_outputs/gr/patch_512/IHC --data_slide_dir /workdir/shared/cpm4c/GR_DATA/IHC --csv_path slides.csv --feat_dir ./clam_outputs/gr/patch_512/IHC --batch_size 32 --slide_ext .ndpi

#########################
# GR HES 10x 
#########################

python CLAM/extract_features_fp.py --data_h5_dir ./clam_outputs/gr/patch_256/HES --data_slide_dir /workdir/shared/cpm4c/GR_DATA/HES --csv_path slides.csv --feat_dir ./clam_outputs/gr/patch_256/HES --batch_size 32 --slide_ext .ndpi
python CLAM/extract_features_fp.py --data_h5_dir ./clam_outputs/gr/patch_256/IHC --data_slide_dir /workdir/shared/cpm4c/GR_DATA/IHC --csv_path slides.csv --feat_dir ./clam_outputs/gr/patch_256/IHC --batch_size 32 --slide_ext .ndpi

