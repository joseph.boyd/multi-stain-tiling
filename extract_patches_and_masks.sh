#!/bin/bash
#SBATCH --job-name=extract_patches
##SBATCH --time=06:00:00
#SBATCH --output=./outputs/%x_%j.out
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --gres=gpu:1
#SBATCH --partition=gpu
##SBATCH --partition=mem
#SBATCH --mem=150000

# Activate anaconda environment code
source activate $WORKDIR/miniconda3/envs/clam

############################
# 10x
############################
#python extract_patches_and_masks.py ./gr_files_patches.csv --coord_path ./CLAM/clam_outputs/gr/patch_1024 --save_path /workdir/shared/cpm4c/stain_transfer/10x --level 2 --mask_level 2

############################
# 20x
############################
python extract_patches_and_masks.py ./gr_files_patches.csv --coord_path ../twin_tiles/CLAM/clam_outputs/gr/patch_512 --save_path ./tiles/20x --level 1 --mask_level 2

############################
# 40x
############################
#python extract_patches_and_masks.py ./gr_files_patches.csv --coord_path ./CLAM/clam_outputs/gr/patch_256 --save_path /workdir/shared/cpm4c/stain_transfer/40x --level 0 --mask_level 2
